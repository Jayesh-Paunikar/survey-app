<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubmitAnswer;
use App\Http\Resources\QuestionResource;
use App\Models\Option;
use App\Models\Question;
use App\Models\UserAnswer;
use Illuminate\Http\Request;

class ServeyController extends Controller
{
    public function index(){
        $question = Question::with('options')->first();
        return new QuestionResource($question);
    }

    public function submit_answer(SubmitAnswer $request){

        $request = $request->validated();
        $option = Option::where('id',$request['option_id'])->first();
        $request['user_id'] = auth()->user()->id;
        $where = ['user_id'=>$request['user_id'],'question_id'=>$request['question_id']];
        UserAnswer::updateOrCreate($where,$request);
        $data = $this->calculate_result($request['question_id']);
        return response()->json(['success'=> "Answer = $option->option submitted successfully",'data'=>$data]);

    }

    public function calculate_result($question_id){

        $total = Question::with('options')->withCount('selected_options')->where('id',$question_id)->first();

        foreach ($total->options as $key => $option) {
            $per = 0;
            if($option->selected_count > 0){
                $per = $option->selected_count / $total->selected_options_count * 100;
            }
            $option->percentage = $per;
            $total->options[$key] = $option;
        }

        return $total;

    }


}
