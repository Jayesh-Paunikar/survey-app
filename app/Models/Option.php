<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $appends = ['selected_count'];

    protected $fillable = [
        'question_id',
        'option'
    ];

    public function getSelectedCountAttribute(){
        return $this->selected_options()->count();
    }

    public function selected_options(){
        return $this->hasMany(UserAnswer::class,'option_id','id');
    }

}
