import axios from 'axios';
import store from './store/index.js';

class Auth {
    constructor () {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.token;
    }
    login (token, user) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.token;
    }
    check () {
        return store.state.token;
    }
    logout () {
        // window.localStorage.clear();
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('user');
        this.user = null;
    }
}
export default new Auth();
