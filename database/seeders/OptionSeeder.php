<?php

namespace Database\Seeders;

use App\Models\Option;
use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            ['question_id'=> 1, 'option'=> 'Good'],
            ['question_id'=> 1, 'option'=> 'Fair'],
            ['question_id'=> 1, 'option'=> 'Neutral'],
            ['question_id'=> 1, 'option'=> 'Bad'],
        ];

        collect($options)->each(function($option){
            Option::create($option);
        });
    }
}
