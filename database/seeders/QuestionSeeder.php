<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;


class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create([
            'question'=>'How do you find our service?'
        ]);

    }
}
